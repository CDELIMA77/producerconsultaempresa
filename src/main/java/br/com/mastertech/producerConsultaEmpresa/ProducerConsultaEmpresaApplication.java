package br.com.mastertech.producerConsultaEmpresa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducerConsultaEmpresaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProducerConsultaEmpresaApplication.class, args);
	}

}
