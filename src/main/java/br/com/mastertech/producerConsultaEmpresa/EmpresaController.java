package br.com.mastertech.producerConsultaEmpresa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmpresaController {

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    @PostMapping
    public void create(@RequestBody Empresa empresa) {
        producer.send("spec2-cynthia-carvalho-2", empresa);
        }
}

